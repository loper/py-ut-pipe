FROM python:3
ADD *.py /

CMD [ "python", "-m", "unittest", "discover" ]
