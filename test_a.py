import unittest
import a


class FaTests(unittest.TestCase):

    def test_funa_1(self):
        res = a.funa(6)
        self.assertEqual(res, 36)

        res = a.funa(786)
        self.assertEqual(res, 617796)

    def test_funa_2(self):
        res = a.funa('1234')
        self.assertEqual(res, None)

    def test_funa_3(self):
        res = a.funa('8')
        self.assertNotEqual(res, 64)

    def test_funb_1(self):
        res = a.funb('kajak')
        self.assertEqual(res, 'kajak')

    def test_funb_2(self):
        res = a.funb('mars')
        self.assertEqual(res, 'sram')

    def test_funb_3(self):
        res = a.funb('woda', mode='soft')
        self.assertEqual(res, 'WOD')

        res = a.funb('dlugie_slowo12345', mode='soft')
        self.assertEqual(res, 'DLUGIE_SLOWO1234')

    def test_funb_4(self):
        res = a.funb(1234, mode='soft')
        self.assertEqual(res, None)

if __name__ == '__main__':
        unittest.main()
