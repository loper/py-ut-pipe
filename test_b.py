import unittest
import b


class FbTests(unittest.TestCase):

    def test_funa_1(self):
        res = b.extreme(21, 38)
        self.assertEqual(res, 63)

        res = b.extreme(22, 18)
        self.assertEqual(res, 28)

        res = b.extreme(22, 18)
        self.assertEqual(res, 28)

    def test_funa_2(self):
        for i in range(5, 10):
            res = b.extreme(i*3, i) % 7
            self.assertEqual(res, 0)

    def test_funa_3(self):
        res = b.extreme(2, 7, 11, 9)
        self.assertGreater(res, 1.63)

        res = b.extreme(987, 10000, 39, 12)
        self.assertGreater(res, 303)
        self.assertLess(res, 304)

    def test_funa_4(self):
        res = b.extreme('k', '0', [1, 2, 4])
        self.assertEqual(res, None)

        res = b.extreme(-3, -2, 99, 881)
        self.assertLess(res, -26)

if __name__ == '__main__':
        unittest.main()
