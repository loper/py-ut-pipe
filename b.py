

def extreme(a, b, c=7, d=3):
    try:
        if a > b:
            return (a-b)*c
        if c > 10:
            return a*d/c
        return (a+b+c-d)
    except TypeError:
        return None
