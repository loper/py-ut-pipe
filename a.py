

def funa(val):
    try:
        return val**2
    except:
        return None


def funb(name, mode='hard'):
    if type(name) is not str:
        return None
    if mode == 'hard':
        return name[::-1]
    else:
        return name.upper()[:-1]
